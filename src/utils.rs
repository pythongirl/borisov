use anyhow::Result;
use std::path;
use tokio::io::{self, AsyncWriteExt};
use tokio::{fs, task};

pub async fn print<S: AsRef<str>>(message: S) -> Result<()> {
    let s = format!("{}\n", message.as_ref());
    let mut stdout = io::stdout();
    stdout.write_all(s.as_ref()).await?;
    stdout.flush().await?;
    Ok(())
}

pub async fn exists(p: impl AsRef<path::Path> + Send + 'static) -> Result<bool> {
    Ok(task::spawn_blocking(move || p.as_ref().exists()).await?)
}

#[cfg(unix)]
pub async fn symlink_file(src: impl AsRef<path::Path>, dst: impl AsRef<path::Path>) -> Result<()> {
    Ok(fs::os::unix::symlink(src, dst).await?)
}

#[cfg(not(unix))]
pub async fn symlink_file(src: impl AsRef<path::Path>, dst: impl AsRef<path::Path>) -> Result<()> {
    Ok(fs::os::windows::symlink_file(src, dst).await?)
}

#[cfg(not(any(unix, windows)))]
compile_error!("Platform must be unix or windows.");
