use crate::utils::exists;
use crate::utils::symlink_file;
use anyhow::Result;
use std::path;
use tokio::fs;

const EULA_FILENAME: &str = "eula.txt";
const JAR_FILENAME: &str = "minecraft_server.jar";
const SERVER_PROPERTIES_FILENAME: &str = "server.properties";

pub struct BaseDir(pub path::PathBuf);

impl BaseDir {
    pub fn data_dir(&self) -> path::PathBuf {
        self.as_path().join("data")
    }

    pub fn static_dir(&self) -> path::PathBuf {
        self.as_path().join("static")
    }

    pub fn jar_path(&self) -> path::PathBuf {
        self.static_dir().join(JAR_FILENAME)
    }

    pub fn eula_path(&self) -> path::PathBuf {
        self.static_dir().join(EULA_FILENAME)
    }

    pub fn server_properties_path(&self) -> path::PathBuf {
        self.static_dir().join(SERVER_PROPERTIES_FILENAME)
    }

    pub fn logger_properties_path(&self) -> path::PathBuf {
        self.static_dir().join(crate::logging::LOGGER_CONFIG_FILENAME)
    }

    pub async fn create_dirs(&self) -> Result<()> {
        if !exists(self.data_dir()).await? {
            fs::create_dir(self.data_dir()).await?;
        }
        if !exists(self.static_dir()).await? {
            fs::create_dir(self.static_dir()).await?;
        }
        Ok(())
    }

    pub async fn create_links(&self) -> Result<()> {
        let eula_link_path = self.data_dir().join(EULA_FILENAME);
        if !exists(eula_link_path.clone()).await? {
            symlink_file(self.eula_path(), eula_link_path.clone()).await?;
        }
        let server_properties_link_path = self.data_dir().join(SERVER_PROPERTIES_FILENAME);
        if !exists(server_properties_link_path.clone()).await? {
            symlink_file(
                self.server_properties_path(),
                server_properties_link_path.clone(),
            )
            .await?;
        }
        Ok(())
    }

    pub async fn remove_links(&self) -> Result<()> {
        tokio::try_join!(
            fs::remove_file(self.data_dir().join(EULA_FILENAME)),
            fs::remove_file(self.data_dir().join(SERVER_PROPERTIES_FILENAME)),
        )?;
        Ok(())
    }

    pub fn as_path(&self) -> &path::Path {
        self.as_ref()
    }
}

impl AsRef<path::Path> for BaseDir {
    fn as_ref(&self) -> &path::Path {
        &self.0
    }
}

/*
Create directories:

static/ # RO config files
    eula.txt
    server.properties
    minecraft_server.<version>.jar

data/ # Game files
    world/
    logs/
    banned-ips.json
    banned-players.json
    ops.json
    whitelist.json
    usercache.json
    symlink server.properties # runtime only
    symlink eula.txt # runtime only
*/
