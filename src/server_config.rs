use serde::{Deserialize, Serialize};
use std::collections;
use std::net;

fn default_bind_address() -> net::SocketAddr {
    "0.0.0.0:25565".parse().unwrap()
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "kebab-case")]
pub struct Config {
    pub mc_version: String,
    pub java_path: Option<String>,
    pub java_options: Option<Vec<String>>,
    #[serde(default = "default_bind_address")]
    pub bind_address: net::SocketAddr,
    #[serde(rename = "passthrough")]
    pub passthrough_server_config: Option<collections::HashMap<String, String>>,
}

impl Config {
    pub fn generate_server_properties(&self) -> collections::HashMap<String, String> {
        let mut properties = collections::HashMap::new();
        // Extra options go here, like listen_address setting
        properties.insert("server-ip".to_string(), self.bind_address.ip().to_string());
        properties.insert(
            "server-port".to_string(),
            self.bind_address.port().to_string(),
        );
        if let Some(passthrough_config) = &self.passthrough_server_config {
            for (k, v) in passthrough_config {
                properties.insert(k.clone(), v.clone());
            }
        }
        properties
    }
}
