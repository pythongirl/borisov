use crate::files::BaseDir;
use crate::utils::print;
use anyhow::Result;
use serde::Deserialize;
use std::fmt;
use tokio::{
    fs,
    io::{self, AsyncBufReadExt},
    stream::StreamExt,
};

pub const LOGGER_CONFIG_FILENAME: &str = "logger.properties";
const LOGGER_CONFIG_FILE_CONTENTS: &str = include_str!("logger.properties");

pub async fn write_logger_config(base_dir: &BaseDir) -> Result<()> {
    fs::write(
        base_dir.logger_properties_path(),
        LOGGER_CONFIG_FILE_CONTENTS,
    )
    .await?;
    Ok(())
}

pub fn logger_java_option(base_dir: &BaseDir) -> String {
    format!(
        "-Dlog4j.configurationFile=file:{}",
        base_dir.logger_properties_path().display()
    )
}

// Maybe define log levels
// https://logging.apache.org/log4j/log4j-2.7/manual/customloglevels.html

#[derive(Deserialize)]
pub struct LogMessage {
    #[serde(with = "chrono::serde::ts_milliseconds")]
    time: chrono::DateTime<chrono::Utc>,
    thread: String,
    level: String,
    message: String,
}

impl fmt::Display for LogMessage {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "[{time}] [{thread}/{level}]: {message}",
            time = self.time.to_rfc3339(),
            thread = &self.thread,
            level = &self.level,
            message = &self.message,
        )
    }
}

pub async fn process_logs(input: impl io::AsyncRead + Unpin) -> Result<()> {
    let buf_reader = io::BufReader::new(input);
    let mut lines = buf_reader.lines();

    while let Some(result) = lines.next().await {
        match result {
            Ok(line) => {
                let log_message: LogMessage = serde_json::from_str(&line)?;
                print(log_message.to_string()).await?
                // print(line).await?;
            }
            Err(_err) => {
                print("error encountered").await?;
            }
        }
    }

    Ok(())
}
